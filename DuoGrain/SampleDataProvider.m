//
//  SampleDataProvider.m
//  DuoGrain
//
//  Created by Barry McNamara on 18/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "SampleDataProvider.h"

@implementation SampleDataProvider

+ (void)loadSamplesFromAsset:(AVAsset *)asset
             completionBlock:(SampleDataCompletionBlock)completion {
    
    NSString *tracks = @"tracks";
    
    [asset loadValuesAsynchronouslyForKeys:@[tracks] completionHandler:^{
        AVKeyValueStatus status = [asset statusOfValueForKey:tracks error:nil];
        
        NSData *sampleData = nil;
        
        if (status == AVKeyValueStatusLoaded) {
            sampleData = [self readAudioSamplesFromAsset:asset];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(sampleData);
        });

    }];
    
}


/**
 *  Reads audio samples from AVAsset containing audio file
 *
 *  @param asset - instance of AVAsset containing audio file
 *
 *  @return - audio samples as instance of NSMutableData
 */
+ (NSData *)readAudioSamplesFromAsset:(AVAsset *)asset {
    
    NSError *error = nil;
    
    AVAssetReader *assetReader = [[AVAssetReader alloc] initWithAsset:asset error:&error];
    
    if (!assetReader) {
        NSLog(@"Error creating asset reader: %@", [error localizedDescription]);
        return nil;
    }
    
    // get the first audio track from asset reader
    AVAssetTrack *track = [[asset tracksWithMediaType:AVMediaTypeAudio] firstObject];
    
    // decompression settings for files not in Linear PCM format
    NSDictionary *outputSettings = @{
                                     AVFormatIDKey               : @(kAudioFormatLinearPCM),
                                     AVLinearPCMIsBigEndianKey   : @NO,
                                     AVLinearPCMIsFloatKey		: @NO,
                                     AVLinearPCMBitDepthKey		: @(16)
                                     };
    
    
    AVAssetReaderTrackOutput *trackOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:track
                                                                             outputSettings:outputSettings];
    
    [assetReader addOutput:trackOutput];
    
    [assetReader startReading];
    
    NSMutableData *sampleData = [NSMutableData data];
    
    while (assetReader.status == AVAssetReaderStatusReading) {
        
        // buffer for storing audio samples
        CMSampleBufferRef sampleBuffer = [trackOutput copyNextSampleBuffer];
        
        if (sampleBuffer) {
            
            CMBlockBufferRef blockBufferRef = CMSampleBufferGetDataBuffer(sampleBuffer);
            
            // get size of buffer and create array of 16 bit
            // ints to store audio samples
            size_t length = CMBlockBufferGetDataLength(blockBufferRef);
            SInt16 sampleBytes[length];
            
            // populate array with data from buffer
            CMBlockBufferCopyDataBytes(blockBufferRef, 0, length, sampleBytes);
            
            // add samples to sampleData object
            [sampleData appendBytes:sampleBytes length:length];
            
            // invalidate buffer from further use
            CMSampleBufferInvalidate(sampleBuffer);
            
            // release buffer when done
            CFRelease(sampleBuffer);
        }
    }
    
    // check to see if 
    if (assetReader.status == AVAssetReaderStatusCompleted) {
        return sampleData;
    } else {
        NSLog(@"Failed to read audio samples from asset");
        return nil;
    }
}


@end
