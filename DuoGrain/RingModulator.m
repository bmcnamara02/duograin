//
//  RingModulator.m
//  DuoGrain
//
//  Created by Barry McNamara on 28/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "RingModulator.h"

@implementation RingModulator

- (instancetype)initWithAudioSource:(AKStereoAudio *)audioSource {
    self = [super init];
    if (self) {
        
        [self setupInputsAndControls];
        
        AKOscillator *oscillator = [AKOscillator oscillator];
        oscillator.frequency = _carrierFreq;
        
        AKRingModulator *leftRingModulator  = [AKRingModulator modulationWithInput:audioSource.leftOutput  carrier:oscillator];
        AKRingModulator *rightRingModualtor = [AKRingModulator modulationWithInput:audioSource.rightOutput carrier:oscillator];
        
        AKMix *leftMix = [[AKMix alloc] initWithInput1:audioSource.leftOutput
                                                input2:leftRingModulator
                                               balance:_mix];
        
        AKMix *rightMix = [[AKMix alloc] initWithInput1:audioSource.rightOutput
                                                 input2:rightRingModualtor
                                                balance:_mix];
        
        // Output to global effects processing
        _auxilliaryOut = [AKStereoAudio globalParameter];
        [self assignOutput:_auxilliaryOut to:[[AKStereoAudio alloc] initWithLeftAudio:leftMix
                                                                           rightAudio:rightMix]];
        
        // Reset Inputs
        [self resetParameter:audioSource];
    }
    return self;
}


/**
 *  Setup synth inputs and controls
 */
- (void)setupInputsAndControls {
    _carrierFreq = [self createPropertyWithValue:1000 minimum:0 maximum:20000];
    _mix = [self createPropertyWithValue:0    minimum:0 maximum:1.0];
}


@end
