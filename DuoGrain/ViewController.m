//
//  ViewController.m
//  DuoGrain
//
//  Created by Barry McNamara on 15/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    
    // synth control ivars
    RotaryControl *leftGrainFreqControl;
    RotaryControl *leftGrainDurationControl;
    RotaryControl *leftGrainDensityControl;
    RotaryControl *leftGrainFreqVarControl;
    RotaryControl *leftGrainFreqDistControl;
    RotaryControl *leftGrainPhaseControl;
    RotaryControl *leftGrainPhaseVarControl;
    
    RotaryControl *rightGrainFreqControl;
    RotaryControl *rightGrainDurationControl;
    RotaryControl *rightGrainDensityControl;
    RotaryControl *rightGrainFreqVarControl;
    RotaryControl *rightGrainFreqDistControl;
    RotaryControl *rightGrainPhaseControl;
    RotaryControl *rightGrainPhaseVarControl;
    
    GranularSynth *granularSynth;
    BOOL isGranularInstrumentPlaying;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupGranularSynth];
    [self setupSynthControls];
    [self setupCrossfaderControl];
    [self setupWaveformViews];
    [self setupAudioOutputPlot];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(handleInterruption:)
                   name:AVAudioSessionInterruptionNotification
                 object:[AVAudioSession sharedInstance]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Setup methods
/**
 *  Create instance of granular synth engine
 */
- (void)setupGranularSynth {
    granularSynth = [[GranularSynth alloc] initWithFile:@"AmbEndDays_0013"
                                                 ofType:@"wav"
                                                andFile:@"CinematicCavusStinger02_0167"
                                                 ofType:@"wav"];
    [AKOrchestra addInstrument:granularSynth];
}

- (void)setupCrossfaderControl {
    _mixSlider.property = granularSynth.mix;
}

/**
 *  Set up UI control for granular synth parameters
 */
- (void)setupSynthControls {
    
    // add boder to container views
    for (UIView *view in _views) {
        view.layer.borderColor = [UIColor colorWithRed:(97/255) green:(196.0/255.0) blue:(175.0/255.0) alpha:255.0].CGColor;
        view.layer.borderWidth = 1.0f;
    }
    
    leftGrainFreqControl = [[RotaryControl alloc] initWithFrame:self.leftGrainFreqControlContainer.bounds];
    [self.leftGrainFreqControlContainer addSubview:leftGrainFreqControl];
    leftGrainFreqControl.lineWidth = 6.0;
    leftGrainFreqControl.pointerLength = 8.0;
    leftGrainFreqControl.value = 0.2;
    leftGrainFreqControl.minimumValue = 0.01;
    leftGrainFreqControl.maximumValue = 10.0;
    [leftGrainFreqControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [leftGrainFreqControl addTarget:self action:@selector(changeLeftGrainFreq:) forControlEvents:UIControlEventValueChanged];
    
    leftGrainDurationControl = [[RotaryControl alloc] initWithFrame:self.leftGrainDurationControlContainer.bounds];
    [self.leftGrainDurationControlContainer addSubview:leftGrainDurationControl];
    leftGrainDurationControl.lineWidth = 6.0;
    leftGrainDurationControl.pointerLength = 8.0;
    leftGrainDurationControl.value = 10.0;
    leftGrainDurationControl.minimumValue = 0.1;
    leftGrainDurationControl.maximumValue = 20;
    [leftGrainDurationControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [leftGrainDurationControl addTarget:self action:@selector(changeLeftGrainDuration:) forControlEvents:UIControlEventValueChanged];
    
    leftGrainDensityControl = [[RotaryControl alloc] initWithFrame:self.leftGrainDensityControlContainer.bounds];
    [self.leftGrainDensityControlContainer addSubview:leftGrainDensityControl];
    leftGrainDensityControl.lineWidth = 6.0;
    leftGrainDensityControl.pointerLength = 8.0;
    leftGrainDensityControl.value = 1.0;
    leftGrainDensityControl.minimumValue = 0.1;
    leftGrainDensityControl.maximumValue = 2.0;
    [leftGrainDensityControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [leftGrainDensityControl addTarget:self action:@selector(changeLeftGrainDensity:) forControlEvents:UIControlEventValueChanged];
    
    leftGrainFreqVarControl = [[RotaryControl alloc] initWithFrame:self.leftGrainFreqVarControlContainer.bounds];
    [self.leftGrainFreqVarControlContainer addSubview:leftGrainFreqVarControl];
    leftGrainFreqVarControl.lineWidth = 6.0;
    leftGrainFreqVarControl.pointerLength = 8.0;
    leftGrainFreqVarControl.value = 10.0;
    leftGrainFreqVarControl.minimumValue = 1.0;
    leftGrainFreqVarControl.maximumValue = 20.0;
    [leftGrainFreqVarControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [leftGrainFreqVarControl addTarget:self action:@selector(changeLeftGrainFreqVar:) forControlEvents:UIControlEventValueChanged];

    leftGrainFreqDistControl = [[RotaryControl alloc] initWithFrame:self.leftGrainFreqDistControlContainer.bounds];
    [self.leftGrainFreqDistControlContainer addSubview:leftGrainFreqDistControl];
    leftGrainFreqDistControl.lineWidth = 6.0;
    leftGrainFreqDistControl.pointerLength = 8.0;
    leftGrainFreqDistControl.value = 10.0;
    leftGrainFreqDistControl.minimumValue = 1.0;
    leftGrainFreqDistControl.maximumValue = 20.0;
    [leftGrainFreqDistControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [leftGrainFreqDistControl addTarget:self action:@selector(changeLeftGrainFreqDist:) forControlEvents:UIControlEventValueChanged];
    
    leftGrainPhaseControl = [[RotaryControl alloc] initWithFrame:self.leftGrainPhaseControlContainer.bounds];
    [self.leftGrainPhaseControlContainer addSubview:leftGrainPhaseControl];
    leftGrainPhaseControl.lineWidth = 6.0;
    leftGrainPhaseControl.pointerLength = 8.0;
    leftGrainPhaseControl.value = 0.1;
    leftGrainPhaseControl.minimumValue = 0.01;
    leftGrainPhaseControl.maximumValue = 1.0;
    [leftGrainPhaseControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [leftGrainPhaseControl addTarget:self action:@selector(changeLeftGrainPhase:) forControlEvents:UIControlEventValueChanged];
    
    leftGrainPhaseVarControl = [[RotaryControl alloc] initWithFrame:self.leftGrainPhaseVarControlContainer.bounds];
    [self.leftGrainPhaseVarControlContainer addSubview:leftGrainPhaseVarControl];
    leftGrainPhaseVarControl.lineWidth = 6.0;
    leftGrainPhaseVarControl.pointerLength = 8.0;
    leftGrainPhaseVarControl.value = 0.1;
    leftGrainPhaseVarControl.minimumValue = 0.01;
    leftGrainPhaseVarControl.maximumValue = 1.0;
    [leftGrainPhaseVarControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [leftGrainPhaseVarControl addTarget:self action:@selector(changeLeftGrainPhaseVar:) forControlEvents:UIControlEventValueChanged];
    
    
    rightGrainFreqControl = [[RotaryControl alloc] initWithFrame:self.rightGrainFreqControlContainer.bounds];
    [self.rightGrainFreqControlContainer addSubview:rightGrainFreqControl];
    rightGrainFreqControl.lineWidth = 6.0;
    rightGrainFreqControl.pointerLength = 8.0;
    rightGrainFreqControl.value = 0.2;
    rightGrainFreqControl.minimumValue = 0.01;
    rightGrainFreqControl.maximumValue = 10.0;
    [rightGrainFreqControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [rightGrainFreqControl addTarget:self action:@selector(changeRightGrainFreq:) forControlEvents:UIControlEventValueChanged];
    
    rightGrainDurationControl = [[RotaryControl alloc] initWithFrame:self.rightGrainDurationControlContainer.bounds];
    [self.rightGrainDurationControlContainer addSubview:rightGrainDurationControl];
    rightGrainDurationControl.lineWidth = 6.0;
    rightGrainDurationControl.pointerLength = 8.0;
    rightGrainDurationControl.value = 10.0;
    rightGrainDurationControl.minimumValue = 0.1;
    rightGrainDurationControl.maximumValue = 20;
    [rightGrainDurationControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [rightGrainDurationControl addTarget:self action:@selector(changeRightGrainDuration:) forControlEvents:UIControlEventValueChanged];
    
    rightGrainDensityControl = [[RotaryControl alloc] initWithFrame:self.rightGrainDensityControlContainer.bounds];
    [self.rightGrainDensityControlContainer addSubview:rightGrainDensityControl];
    rightGrainDensityControl.lineWidth = 6.0;
    rightGrainDensityControl.pointerLength = 8.0;
    rightGrainDensityControl.value = 1.0;
    rightGrainDensityControl.minimumValue = 0.1;
    rightGrainDensityControl.maximumValue = 2.0;
    [rightGrainDensityControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [rightGrainDensityControl addTarget:self action:@selector(changeRightGrainDensity:) forControlEvents:UIControlEventValueChanged];
    
    rightGrainFreqVarControl = [[RotaryControl alloc] initWithFrame:self.rightGrainFreqVarControlContainer.bounds];
    [self.rightGrainFreqVarControlContainer addSubview:rightGrainFreqVarControl];
    rightGrainFreqVarControl.lineWidth = 6.0;
    rightGrainFreqVarControl.pointerLength = 8.0;
    rightGrainFreqVarControl.value = 10.0;
    rightGrainFreqVarControl.minimumValue = 1.0;
    rightGrainFreqVarControl.maximumValue = 20.0;
    [rightGrainFreqVarControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [rightGrainFreqVarControl addTarget:self action:@selector(changeRightGrainFreqVar:) forControlEvents:UIControlEventValueChanged];
    
    rightGrainFreqDistControl = [[RotaryControl alloc] initWithFrame:self.rightGrainFreqDistControlContainer.bounds];
    [self.rightGrainFreqDistControlContainer addSubview:rightGrainFreqDistControl];
    rightGrainFreqDistControl.lineWidth = 6.0;
    rightGrainFreqDistControl.pointerLength = 8.0;
    rightGrainFreqDistControl.value = 10.0;
    rightGrainFreqDistControl.minimumValue = 1.0;
    rightGrainFreqDistControl.maximumValue = 20.0;
    [rightGrainFreqDistControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [rightGrainFreqDistControl addTarget:self action:@selector(changeRightGrainFreqDist:) forControlEvents:UIControlEventValueChanged];
    
    rightGrainPhaseControl = [[RotaryControl alloc] initWithFrame:self.rightGrainPhaseControlContainer.bounds];
    [self.rightGrainPhaseControlContainer addSubview:rightGrainPhaseControl];
    rightGrainPhaseControl.lineWidth = 6.0;
    rightGrainPhaseControl.pointerLength = 8.0;
    rightGrainPhaseControl.value = 0.1;
    rightGrainPhaseControl.minimumValue = 0.01;
    rightGrainPhaseControl.maximumValue = 1.0;
    [rightGrainPhaseControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [rightGrainPhaseControl addTarget:self action:@selector(changeRightGrainPhase:) forControlEvents:UIControlEventValueChanged];
    
    rightGrainPhaseVarControl = [[RotaryControl alloc] initWithFrame:self.rightGrainPhaseVarControlContainer.bounds];
    [self.rightGrainPhaseVarControlContainer addSubview:rightGrainPhaseVarControl];
    rightGrainPhaseVarControl.lineWidth = 6.0;
    rightGrainPhaseVarControl.pointerLength = 8.0;
    rightGrainPhaseVarControl.value = 0.1;
    rightGrainPhaseVarControl.minimumValue = 0.01;
    rightGrainPhaseVarControl.maximumValue = 1.0;
    [rightGrainPhaseVarControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    [rightGrainPhaseVarControl addTarget:self action:@selector(changeRightGrainPhaseVar:) forControlEvents:UIControlEventValueChanged];
}

/**
 *  Render waveforms of source files to waveform views
 */
- (void)setupWaveformViews {
    NSString *leftPath = [[NSBundle mainBundle] pathForResource:@"AmbEndDays_0013"
                                                         ofType:@"wav"
                                                    inDirectory:@"AKSoundFiles.bundle/Sounds"];
    NSURL *leftUrl = [NSURL fileURLWithPath:leftPath];
    
    self.leftWaveformView.waveColor = [UIColor colorWithRed:(78.0/255.0) green:(142.0/255.0) blue:(203.0/255.0) alpha:1.0];
    self.leftWaveformView.backgroundColor = [UIColor colorWithRed:(216.0/255.0) green:(216.0/255.0) blue:(210.0/255.0) alpha:1.0];
    self.leftWaveformView.asset = [AVURLAsset assetWithURL:leftUrl];
    
    NSString *rightPath = [[NSBundle mainBundle] pathForResource:@"CinematicCavusStinger02_0167"
                                                          ofType:@"wav"
                                                     inDirectory:@"AKSoundFiles.bundle/Sounds"];
    NSURL *rightUrl = [NSURL fileURLWithPath:rightPath];
    
    self.rightWaveformView.waveColor = [UIColor colorWithRed:(78.0/255.0) green:(142.0/255.0) blue:(203.0/255.0) alpha:1.0];
    self.rightWaveformView.backgroundColor = [UIColor colorWithRed:(216.0/255.0) green:(216.0/255.0) blue:(210.0/255.0) alpha:1.0];

    self.rightWaveformView.asset = [AVURLAsset assetWithURL:rightUrl];
}

- (void)setupAudioOutputPlot {
    
    _audioOutputPlot.layer.borderColor = [UIColor colorWithRed:239.0/255.0 green:58.0/255.0 blue:36.0/255.0 alpha:0.75].CGColor;
    _audioOutputPlot.layer.borderWidth = 1.0;
    
    _audioOutputPlot.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:210.0/255.0 alpha:1.0];
    _audioOutputPlot.plotColor = [UIColor colorWithRed:239.0/255.0 green:58.0/255.0 blue:36.0/255.0 alpha:0.75];
}


#pragma mark - Control methods
/**
 *  Audio on / off
 *
 *  @param sender - UISwitch control
 */
- (IBAction)toggleGranularInstrument:(id)sender {
    if (isGranularInstrumentPlaying) {
        [granularSynth stop];
        isGranularInstrumentPlaying = NO;
    } else {
        [granularSynth play];
        isGranularInstrumentPlaying = YES;
    }
}

- (IBAction)changeLeftGrainFreq:(id)sender {
    [AKTools setProperty:granularSynth.leftFrequency withSlider:(UISlider *)sender];
}

- (IBAction)changeLeftGrainDuration:(id)sender {
    [AKTools setProperty:granularSynth.leftDuration withSlider:(UISlider *)sender];
}

- (IBAction)changeLeftGrainDensity:(id)sender {
    [AKTools setProperty:granularSynth.leftDensity withSlider:(UISlider *)sender];
}

- (IBAction)changeLeftGrainFreqVar:(id)sender {
    [AKTools setProperty:granularSynth.leftFrequencyVariation withSlider:(UISlider *)sender];
}

- (IBAction)changeLeftGrainFreqDist:(id)sender {
    [AKTools setProperty:granularSynth.leftFrequencyDistribution withSlider:(UISlider *)sender];
}

- (IBAction)changeLeftGrainPhase:(id)sender {
    [AKTools setProperty:granularSynth.leftPhase withSlider:(UISlider *)sender];
}

- (IBAction)changeLeftGrainPhaseVar:(id)sender {
    [AKTools setProperty:granularSynth.leftRandomPhaseVar withSlider:(UISlider *)sender];
}

- (IBAction)changeRightGrainFreq:(id)sender {
    [AKTools setProperty:granularSynth.rightFrequency withSlider:(UISlider *)sender];
}

- (IBAction)changeRightGrainDuration:(id)sender {
    [AKTools setProperty:granularSynth.rightDuration withSlider:(UISlider *)sender];
}

- (IBAction)changeRightGrainDensity:(id)sender {
    [AKTools setProperty:granularSynth.rightDensity withSlider:(UISlider *)sender];
}

- (IBAction)changeRightGrainFreqVar:(id)sender {
    [AKTools setProperty:granularSynth.rightFrequencyVariation withSlider:(UISlider *)sender];
}

- (IBAction)changeRightGrainFreqDist:(id)sender {
    [AKTools setProperty:granularSynth.rightFrequencyDistribution withSlider:(UISlider *)sender];
}

- (IBAction)changeRightGrainPhase:(id)sender {
    [AKTools setProperty:granularSynth.rightPhase withSlider:(UISlider *)sender];
}

- (IBAction)changeRightGrainPhaseVar:(id)sender {
    [AKTools setProperty:granularSynth.rightRandomPhaseVar withSlider:(UISlider *)sender];
}

#pragma mark - Interruption Handler

- (void)handleInterruption:(NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    AVAudioSessionInterruptionType interruptionType = [info[AVAudioSessionInterruptionTypeKey] unsignedIntegerValue];
    
    if (interruptionType == AVAudioSessionInterruptionTypeBegan) {
        [granularSynth stop];
    }
    else {
        AVAudioSessionInterruptionOptions options = [info[AVAudioSessionInterruptionOptionKey] unsignedIntegerValue];
        
        if (options == AVAudioSessionInterruptionOptionShouldResume) {
            [granularSynth play];
        }
    }
}

@end
