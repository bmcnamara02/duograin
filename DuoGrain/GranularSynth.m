//
//  GranularSynth.m
//  DuoGrain
//
//  Created by Barry McNamara on 24/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "GranularSynth.h"
#import "AKFoundation.h"
@implementation GranularSynth

/**
 *  Custom initializer, takes two file inputs, one for left channel, one for right
 *
 *  @param file - file name
 *  @param type - file type
 *
 *  @return instance of self
 */
- (instancetype)initWithFile:(NSString *)fileOne
                      ofType:(NSString *)typeOne
                     andFile:(NSString *)fileTwo
                      ofType:(NSString *)typeTwo {
    
    self = [super init];
    
    if (self) {
        
        [self setupInputsAndControls];
        
        NSString *fileName = [AKManager pathToSoundFile:fileOne ofType:typeOne];
        
        // wave table for audio source
        AKSoundFileTable *soundFile = [[AKSoundFileTable alloc] initWithFilename:fileName size:16384];
        
        // instance of granular synth
        AKGranularSynthesizer *leftSynth = [[AKGranularSynthesizer alloc] initWithGrainWaveform:soundFile
                                                                                  frequency:_leftFrequency];
        leftSynth.duration = _leftDuration;
        leftSynth.density = _leftDensity;
        leftSynth.frequencyVariation = _leftFrequencyVariation;
        leftSynth.frequencyVariationDistribution = _leftFrequencyDistribution;
        leftSynth.phase = _leftPhase;
        leftSynth.prpow = _leftRandomPhaseVar;
        
        
        
        NSString *file2 = [AKManager pathToSoundFile:fileTwo ofType:typeTwo];
        AKSoundFileTable *soundFile2 = [[AKSoundFileTable alloc] initWithFilename:file2 size:16384];
        AKGranularSynthesizer *rightSynth = [[AKGranularSynthesizer alloc] initWithGrainWaveform:soundFile2
                                                                                   frequency:_rightFrequency];
        rightSynth.duration = _rightDuration;
        rightSynth.density = _rightDensity;
        rightSynth.frequencyVariation = _rightFrequencyVariation;
        rightSynth.frequencyVariationDistribution = _rightFrequencyDistribution;
        rightSynth.phase = _rightPhase;
        rightSynth.prpow = _rightRandomPhaseVar;
        
//        _auxilliaryOut = [AKStereoAudio globalParameter];
//        [self assignOutput:_auxilliaryOut to:leftSynth];
        
        AKMix *mixer = [[AKMix alloc] initWithInput1:leftSynth
                                              input2:rightSynth
                                             balance:_mix];
        


        
        [self setAudioOutput:[mixer scaledBy:akp(0.5)]];

    }
    
    return self;
}

/**
 *  Setup synth inputs and controls
 */
- (void)setupInputsAndControls {

    _leftFrequency = [self createPropertyWithValue:0.2 minimum:0.01 maximum:10];
    _leftDuration = [self createPropertyWithValue:10  minimum:0.1  maximum:20];
    _leftDensity = [self createPropertyWithValue:1   minimum:0.1  maximum:2];
    _leftFrequencyVariation = [self createPropertyWithValue:10  minimum:0.1  maximum:20];
    _leftFrequencyDistribution = [self createPropertyWithValue:10  minimum:0.1  maximum:20];
    _leftPhase = [self createPropertyWithValue:0.1 minimum:0.01 maximum:1.0];
    _leftRandomPhaseVar = [self createPropertyWithValue:0.0 minimum:0.0 maximum:1.0];
    
    _rightFrequency = [self createPropertyWithValue:0.2 minimum:0.01 maximum:10];
    _rightDuration = [self createPropertyWithValue:10  minimum:0.1  maximum:20];
    _rightDensity = [self createPropertyWithValue:1   minimum:0.1  maximum:2];
    _rightFrequencyVariation = [self createPropertyWithValue:10  minimum:0.1  maximum:20];
    _rightFrequencyDistribution = [self createPropertyWithValue:10  minimum:0.1  maximum:20];
    _rightPhase = [self createPropertyWithValue:0.1 minimum:0.01 maximum:1.0];
    _rightRandomPhaseVar = [self createPropertyWithValue:0.0 minimum:0.0 maximum:1.0];
    
    _mix = [self createPropertyWithValue:0.5 minimum:0    maximum:1];
}

@end
