//
//  ViewController.h
//  DuoGrain
//
//  Created by Barry McNamara on 15/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKFoundation.h"
#import "GranularSynth.h"
#import "RotaryControl.h"
#import "WaveformView.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *views;

@property (weak, nonatomic) IBOutlet UIView *leftGrainFreqControlContainer;
@property (weak, nonatomic) IBOutlet UIView *leftGrainDurationControlContainer;
@property (weak, nonatomic) IBOutlet UIView *leftGrainDensityControlContainer;
@property (weak, nonatomic) IBOutlet UIView *leftGrainFreqVarControlContainer;
@property (weak, nonatomic) IBOutlet UIView *leftGrainFreqDistControlContainer;
@property (weak, nonatomic) IBOutlet UIView *leftGrainPhaseControlContainer;
@property (weak, nonatomic) IBOutlet UIView *leftGrainPhaseVarControlContainer;

@property (weak, nonatomic) IBOutlet UIView *rightGrainFreqControlContainer;
@property (weak, nonatomic) IBOutlet UIView *rightGrainDurationControlContainer;
@property (weak, nonatomic) IBOutlet UIView *rightGrainDensityControlContainer;
@property (weak, nonatomic) IBOutlet UIView *rightGrainFreqVarControlContainer;
@property (weak, nonatomic) IBOutlet UIView *rightGrainFreqDistControlContainer;
@property (weak, nonatomic) IBOutlet UIView *rightGrainPhaseVarControlContainer;
@property (weak, nonatomic) IBOutlet UIView *rightGrainPhaseControlContainer;

@property (weak, nonatomic) IBOutlet AKPropertySlider *mixSlider;

@property (weak, nonatomic) IBOutlet WaveformView *leftWaveformView;
@property (weak, nonatomic) IBOutlet WaveformView *rightWaveformView;

@property (weak, nonatomic) IBOutlet AKAudioOutputRollingWaveformPlot *audioOutputPlot;
@property (weak, nonatomic) IBOutlet UIView *plotContainerView;


@end

