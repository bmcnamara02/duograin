//
//  GranularSynth.h
//  DuoGrain
//
//  Created by Barry McNamara on 24/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "AKInstrument.h"

@interface GranularSynth : AKInstrument

- (instancetype)initWithFile:(NSString *)fileOne
                      ofType:(NSString *)typeOne
                     andFile:(NSString *)fileTwo
                      ofType:(NSString *)typeTwo;

@property AKInstrumentProperty *leftFrequency;
@property AKInstrumentProperty *leftDuration;
@property AKInstrumentProperty *leftDensity;
@property AKInstrumentProperty *leftFrequencyVariation;
@property AKInstrumentProperty *leftFrequencyDistribution;
@property AKInstrumentProperty *leftPhase;
@property AKInstrumentProperty *leftRandomPhaseVar;

@property AKInstrumentProperty *rightFrequency;
@property AKInstrumentProperty *rightDuration;
@property AKInstrumentProperty *rightDensity;
@property AKInstrumentProperty *rightFrequencyVariation;
@property AKInstrumentProperty *rightFrequencyDistribution;
@property AKInstrumentProperty *rightPhase;
@property AKInstrumentProperty *rightRandomPhaseVar;

@property AKInstrumentProperty *mix;

@property (readonly) AKStereoAudio *auxilliaryOut;

@end
