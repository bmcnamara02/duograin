//
//  RotationGestureHandler.m
//  DuoGrain
//
//  Created by Barry McNamara on 19/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "RotationGestureHandler.h"
#import <UIKit/UIGestureRecognizerSubclass.h>

@implementation RotationGestureHandler

- (id)initWithTarget:(id)target action:(SEL)action {
    self = [super initWithTarget:target action:action];
    if(self) {
        // ensure control works with only one touch at a time
        self.maximumNumberOfTouches = 1;
        self.minimumNumberOfTouches = 1;
    }
    return self;
}

#pragma mark - Gesture recognizer method overrides
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self updateTouchAngleWithTouches:touches];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    [self updateTouchAngleWithTouches:touches];
}

#pragma mark - Utility methods
/**
 *  maps touch point to containing view cooridinate system
 *  and updates touchAngle via calculateAngleToPoint
 *
 *  @param touches - NSSet of touch events
 */
- (void)updateTouchAngleWithTouches:(NSSet *)touches {
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
    
    self.touchAngle = [self calculateAngleToPoint:touchPoint];
}

/**
 *  calculates touchAngle value
 *
 *  @param point - point of touch event
 *
 *  @return - CGFloat representing touchAngle
 */
- (CGFloat)calculateAngleToPoint:(CGPoint)point {
    // Offset by the center
    CGPoint centerOffset = CGPointMake(point.x - CGRectGetMidX(self.view.bounds),
                                       point.y - CGRectGetMidY(self.view.bounds));
    return atan2(centerOffset.y, centerOffset.x);
}

@end
