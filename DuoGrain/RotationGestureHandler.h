//
//  RotationGestureHandler.h
//  DuoGrain
//
//  Created by Barry McNamara on 19/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RotationGestureHandler : UIPanGestureRecognizer

@property (nonatomic, assign) CGFloat touchAngle;

@end
