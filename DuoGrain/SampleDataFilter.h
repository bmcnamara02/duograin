//
//  SampleDataFilter.h
//  DuoGrain
//
//  Created by Barry McNamara on 18/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

// This class is used to reduce the sample data from the audio asset read by the SampleDataProvider class
// so that the sample data can be rendered on screen using the WaveformView class

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface SampleDataFilter : NSObject

/**
 *  Custom initializer
 *
 *  @param sampleData - NSData object containing audio samples from SampleDataProvider class
 *
 *  @return - instance of self
 */
- (id)initWithData:(NSData *)sampleData;


/**
 *  Reduces the set of audio sample data for the purpose of rendering with WaveformView
 *
 *  @param size - size constraint for reducing audio data, derived from 
 *
 *  @return - array containing reduced audio data
 */
- (NSArray *)reducedSampleDataForSize:(CGSize)size;

@end
