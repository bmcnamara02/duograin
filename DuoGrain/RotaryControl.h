//
//  RotaryControl.h
//  DuoGrain
//
//  Created by Barry McNamara on 19/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RotaryControl : UIControl

@property (nonatomic, assign) CGFloat value;
@property (nonatomic, assign) CGFloat maximumValue;
@property (nonatomic, assign) CGFloat minimumValue;
@property (nonatomic, assign, getter = isContinuous) BOOL continuous;
@property (nonatomic, assign) CGFloat startAngle;
@property (nonatomic, assign) CGFloat endAngle;
@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) CGFloat pointerLength;


/**
 *  Set control value
 *
 *  @param value    - control value
 *  @param animated - optional animation value
 */
- (void)setValue:(CGFloat)value animated:(BOOL)animated;

@end
