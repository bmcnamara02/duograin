//
//  AppDelegate.h
//  DuoGrain
//
//  Created by Barry McNamara on 15/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

