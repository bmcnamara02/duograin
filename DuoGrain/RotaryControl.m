//
//  RotaryControl.m
//  DuoGrain
//
//  Created by Barry McNamara on 19/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "RotaryControl.h"
#import "RotaryControlRenderer.h"
#import "RotationGestureHandler.h"

@implementation RotaryControl {
    RotaryControlRenderer *_renderer;
    RotationGestureHandler *_gestureHandler;
}

/**
 *  no need to synthesise these properites
 *  as custom accessors will be provided
 */
@dynamic lineWidth;
@dynamic startAngle;
@dynamic endAngle;
@dynamic pointerLength;

#pragma mark - Initializer
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
//        _minimumValue = 0.0;
//        _maximumValue = 1.0;
//        _value = 0.0;
        _continuous = YES;
        
        [self createControlUI];
        [self setupGestureRecogniser];
    }
    return self;
}

#pragma mark - API Methods
- (void)setValue:(CGFloat)value animated:(BOOL)animated {
    if(value != _value) {
        _value = MIN(self.maximumValue, MAX(self.minimumValue, value));
        
        // Update the control with the correct angle
        CGFloat angleRange = self.endAngle - self.startAngle;
        CGFloat valueRange = self.maximumValue - self.minimumValue;
        CGFloat angleForValue = (_value - self.minimumValue) / valueRange * angleRange + self.startAngle;
        [_renderer setPointerAngle:angleForValue animated:animated];
        [self didChangeValueForKey:@"value"];
    }
}

#pragma mark - Accessor overrides
- (void)setValue:(CGFloat)value {
    [self setValue:value animated:NO];
}

- (CGFloat)lineWidth {
    return _renderer.lineWidth;
}

- (void)setLineWidth:(CGFloat)lineWidth {
    _renderer.lineWidth = lineWidth;
}

- (CGFloat)startAngle {
    return _renderer.startAngle;
}

- (void)setStartAngle:(CGFloat)startAngle {
    _renderer.startAngle = startAngle;
}

- (CGFloat)endAngle {
    return _renderer.endAngle;
}

- (void)setEndAngle:(CGFloat)endAngle {
    _renderer.endAngle = endAngle;
}

- (CGFloat)pointerLength {
    return _renderer.pointerLength;
}

- (void)setPointerLength:(CGFloat)pointerLength {
    _renderer.pointerLength = pointerLength;
}

/**
 *  Set up control renderer
 */
- (void)createControlUI {
    
    _renderer = [[RotaryControlRenderer alloc] init];
    [_renderer updateWithBounds:self.bounds];
    _renderer.color = [UIColor colorWithRed:(97/255) green:(196.0/255.0) blue:(175.0/255.0) alpha:255.0];
    _renderer.startAngle = -M_PI * 11 / 8.0;
    _renderer.endAngle = M_PI * 3 / 8.0;
    _renderer.pointerAngle = _renderer.startAngle;
    [self.layer addSublayer:_renderer.trackLayer];
    [self.layer addSublayer:_renderer.pointerLayer];
    
}

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)key {
    // Handle KVO notifications for the value property ourselves, since
    // we're proxying with the setValue:animated: method.
    if ([key isEqualToString:@"value"]) {
        return NO;
    } else {
        return [super automaticallyNotifiesObserversForKey:key];
    }
}

#pragma mark - Gesture handlers
- (void)setupGestureRecogniser {
    _gestureHandler = [[RotationGestureHandler alloc] initWithTarget:self action:@selector(handleGesture:)];
    [self addGestureRecognizer:_gestureHandler];

}


/**
 *  Handle gesture interaction
 *
 *  @param gesture - user interaction
 */
- (void)handleGesture:(RotationGestureHandler *)gesture {
    // Mid-point angle
    CGFloat midPointAngle = (2 * M_PI + self.startAngle - self.endAngle) / 2 + self.endAngle;
    
    // Ensure the angle is within a suitable range
    CGFloat boundedAngle = gesture.touchAngle;
    if(boundedAngle > midPointAngle) {
        boundedAngle -= 2 * M_PI;
    } else if (boundedAngle < (midPointAngle - 2 * M_PI)) {
        boundedAngle += 2 * M_PI;
    }
    // Bound the angle to within the suitable range
    boundedAngle = MIN(self.endAngle, MAX(self.startAngle, boundedAngle));
    
    // Convert the angle to a value
    CGFloat angleRange = self.endAngle - self.startAngle;
    CGFloat valueRange = self.maximumValue - self.minimumValue;
    CGFloat valueForAngle = (boundedAngle - self.startAngle) / angleRange * valueRange + self.minimumValue;
    
    // Set the control to this value
    self.value = valueForAngle;
    
    // Notify of value change
    if (self.continuous) {
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    } else {
        // Only send an update if the gesture has completed
        if(_gestureHandler.state == UIGestureRecognizerStateEnded
           || _gestureHandler.state == UIGestureRecognizerStateCancelled) {
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        }
    }
}

@end
