//
//  WaveformView.h
//  DuoGrain
//
//  Created by Barry McNamara on 18/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AVAsset;

@interface WaveformView : UIView

@property (nonatomic, strong) AVAsset *asset;
@property (nonatomic, strong) UIColor *waveColor;

@end
