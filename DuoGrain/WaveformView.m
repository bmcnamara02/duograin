//
//  WaveformView.m
//  DuoGrain
//
//  Created by Barry McNamara on 18/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "WaveformView.h"
#import "SampleDataFilter.h"
#import "SampleDataProvider.h"
#import <QuartzCore/QuartzCore.h>

static const CGFloat widthScale = 0.95;
static const CGFloat heightScale = 0.95;

@interface WaveformView()

@property (strong, nonatomic) UIActivityIndicatorView *loadingView;
@property (nonatomic, strong) SampleDataFilter *filter;

@end

@implementation WaveformView

#pragma mark - Initializers
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setupView];
    }
    return self;
}

#pragma mark - Setup methods
- (void)setupView {
    self.backgroundColor = [UIColor clearColor];
    self.waveColor = [UIColor whiteColor];
    self.layer.cornerRadius = 2.0f;
    self.layer.masksToBounds = YES;
    
    UIActivityIndicatorViewStyle style = UIActivityIndicatorViewStyleWhiteLarge;
    
    _loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    
    CGSize size = _loadingView.frame.size;
    CGFloat x = (self.bounds.size.width - size.width) / 2;
    CGFloat y = (self.bounds.size.height - size.height) / 2;
    _loadingView.frame = CGRectMake(x, y, size.width, size.height);
    [self addSubview:_loadingView];
    
    [_loadingView startAnimating];
}

- (void)setWaveColor:(UIColor *)waveColor {
    _waveColor = waveColor;
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = waveColor.CGColor;
    [self setNeedsDisplay];
}

- (void)setAsset:(AVAsset *)asset {
    if (_asset != asset) {
        _asset = asset;
        
        // load samples from asset using SampleDataProvider class
        [SampleDataProvider loadSamplesFromAsset:self.asset
                                 completionBlock:^(NSData *sampleData) {
                                     
                                     // create instance of SampleDataFilter and pass in NSData object containing audio samples
                                     self.filter = [[SampleDataFilter alloc] initWithData:sampleData];
                                     
                                     // dismiss HUD and invoke drawRect()
                                     [self.loadingView stopAnimating];
                                     [self setNeedsDisplay];
                                 }];
    }
}

#pragma mark - Draw method
- (void)drawRect:(CGRect)rect {
    
    // get current graphics context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // scale context by width and height constants
    CGContextScaleCTM(context, widthScale, heightScale);
    
    CGFloat xOffset = self.bounds.size.width - (self.bounds.size.width * widthScale);
    
    CGFloat yOffset = self.bounds.size.height - (self.bounds.size.height * heightScale);
    
    CGContextTranslateCTM(context, xOffset / 2, yOffset / 2);
    
    // array from reduced audio samples buffer
    NSArray *filteredSamples = [self.filter reducedSampleDataForSize:self.bounds.size];
    
    CGFloat midY = CGRectGetMidY(rect);
    
    // CGMutablePathRef used to draw the top half of the waveform bezier path
    CGMutablePathRef halfPath = CGPathCreateMutable();
    CGPathMoveToPoint(halfPath, NULL, 0.0f, midY);
    
    // iterate over array of samples and points to path
    for (NSUInteger i = 0; i < filteredSamples.count; i++) {
        float sample = [filteredSamples[i] floatValue];
        CGPathAddLineToPoint(halfPath, NULL, i, midY - sample);
    }
    
    // add lines to points
    CGPathAddLineToPoint(halfPath, NULL, filteredSamples.count, midY);
    
    // create new CGMutablePathRef with top half path
    CGMutablePathRef fullPath = CGPathCreateMutable();
    CGPathAddPath(fullPath, NULL, halfPath);
    
    // translate and scale top half of waveform to render full waveform
    CGAffineTransform transform = CGAffineTransformIdentity;
    transform = CGAffineTransformTranslate(transform, 0, CGRectGetHeight(rect));
    transform = CGAffineTransformScale(transform, 1.0, -1.0);
    CGPathAddPath(fullPath, &transform, halfPath);
    
    // add full waveform path to graphics context, set color
    CGContextAddPath(context, fullPath);
    CGContextSetFillColorWithColor(context, self.waveColor.CGColor);
    CGContextDrawPath(context, kCGPathFill);
    
    // release paths from memory
    CGPathRelease(halfPath);
    CGPathRelease(fullPath);
}


@end
