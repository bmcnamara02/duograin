//
//  RingModulator.h
//  DuoGrain
//
//  Created by Barry McNamara on 28/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "AKInstrument.h"
#import "AKFoundation.h"

@interface RingModulator : AKInstrument

@property (readonly) AKStereoAudio *auxilliaryOut;

@property (nonatomic, strong) AKInstrumentProperty *carrierFreq;
@property (nonatomic, strong) AKInstrumentProperty *mix;


/**
 *  Custom initializer
 *
 *  @param audioSource - input signal
 *
 *  @return instance of Ring Mod
 */
- (instancetype)initWithAudioSource:(AKStereoAudio *)audioSource;

@end
