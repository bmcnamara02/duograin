//
//  RotaryControlRenderer.h
//  DuoGrain
//
//  Created by Barry McNamara on 19/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RotaryControlRenderer : NSObject

@property (nonatomic, strong) UIColor *color;
@property (nonatomic, assign) CGFloat lineWidth;

// track layer properties
@property (nonatomic, readonly, strong) CAShapeLayer *trackLayer;
@property (nonatomic, assign) CGFloat startAngle;
@property (nonatomic, assign) CGFloat endAngle;

// pointer layer properties
@property (nonatomic, readonly, strong) CAShapeLayer *pointerLayer;
@property (nonatomic, assign) CGFloat pointerAngle;
@property (nonatomic, assign) CGFloat pointerLength;

/**
 *  resizes CALayers and positions them in the
 *  centre of the bounding rectangle
 *
 *  @param bounds - bounds of containing UIView
 */
- (void)updateWithBounds:(CGRect)bounds;

/**
 *  set angle of pointer layer
 *
 *  @param pointerAngle - pointer angle
 *  @param animated - optional animation value
 */
- (void)setPointerAngle:(CGFloat)pointerAngle animated:(BOOL)animated;

@end
