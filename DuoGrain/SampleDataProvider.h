//
//  SampleDataProvider.h
//  DuoGrain
//
//  Created by Barry McNamara on 18/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//


// This class extracts all samples from a given audio asset.
// The sample data will be reduced using the SampleDataFilter class
// and rendered to screen using the WaveformView class

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

typedef void(^SampleDataCompletionBlock)(NSData *);

@interface SampleDataProvider : NSObject

/**
 *  loads sample data from instance of AVAsset
 *
 *  @param asset      - instance of AVAsset containing audio track to be rendered
 *  @param completion  - block called on completion of load process
 */
+ (void)loadSamplesFromAsset:(AVAsset *)asset
             completionBlock:(SampleDataCompletionBlock)completion;

@end
