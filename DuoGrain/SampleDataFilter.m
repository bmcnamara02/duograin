//
//  SampleDataFilter.m
//  DuoGrain
//
//  Created by Barry McNamara on 18/08/2015.
//  Copyright (c) 2015 Barry McNamara. All rights reserved.
//

#import "SampleDataFilter.h"

@interface SampleDataFilter()

@property (nonatomic, strong)  NSData *sampleData;

@end

@implementation SampleDataFilter

- (id)initWithData:(NSData *)sampleData {
    self = [super init];
    
    if (self) {
        _sampleData = sampleData;
    }
    
    return self;
}

- (NSArray *)reducedSampleDataForSize:(CGSize)size {
    
    // array to hold reduced number of audio samples
    NSMutableArray *reducedSamples = [[NSMutableArray alloc] init];
    
    // calculate a bin size for size constraint of containing view
    NSUInteger sampleCount = self.sampleData.length / sizeof(SInt16);
    NSUInteger binSize = sampleCount / size.width;
    
    SInt16 *bytes = (SInt16 *)self.sampleData.bytes;
    
    SInt16 maxSample = 0;
    
    for (NSUInteger i = 0; i < sampleCount; i += binSize) {
        
        SInt16 sampleBin[binSize];
        
        // iterate over sample data and create bin of data to be processed
        for (NSUInteger j = 0; j < binSize; j++) {
            sampleBin[j] = CFSwapInt16LittleToHost(bytes[i + j]);
        }
        
        // find the maximum sample value for each bin
        SInt16 value = [self maximumValueInArray:sampleBin ofSize:binSize];
        [reducedSamples addObject:@(value)];
        
        // find the maximum value in reduced sample values
        if (value > maxSample) {
            maxSample = value;
        }
    }
    
    // scale values according to the size of the rendering view
    CGFloat scaleFactor = (size.height / 2) / maxSample;
    
    for (NSUInteger i = 0; i < reducedSamples.count; i++) {
        reducedSamples[i] = @([reducedSamples[i] integerValue] * scaleFactor);
    }
    
    return reducedSamples;
}

/**
 *  returns maximum sample size for given bin of samples
 *
 *  @param values - array of audio samples
 *  @param size   - size of sample bin
 *
 *  @return signed sixteen bit integer representing maximum sample value
 */
- (SInt16)maximumValueInArray:(SInt16[])values ofSize:(NSUInteger)size {
    SInt16 maxValue = 0;
    for (int i = 0; i < size; i++) {
        if (abs(values[i]) > maxValue) {
            maxValue = abs(values[i]);
        }
    }
    return maxValue;
}

@end
